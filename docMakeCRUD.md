# CRUD CREATOR > WIP

CRUD Creator permet de créer des routes, controllers, singles et formulaires CRUD facilement en quelques prompts.


## Utilisation

À la base du projet, veuillez créer un dossier bin -> console.php.

Celui-ci sera la fichier dans lequel vous initierez l'objet MakeCRUD().

Creéz ensuite un dossier Console dans core dans lequel sera l'objet MakeCRUD().

Faites un require de MakeCRUD.php dans console.php pour pouvoir l'appeler >>>

```bash
require ('core/Console/MakeCRUD.php');
```

Pour initier l'objet, rentrez cette ligne dans le terminal à la base du projet >>>

```bash
php bin/console.php make controller
```

## Présentation

CRUD Creator se présente avec plusieurs méthodes permettant de créer tout la structure souhaitée par l'utilisateur.
Celui-ci se compose de run() qui contient elle-même toutes les méthodes qui remplient selon les prompts comme ci-dessous :

#### /!\ Les instructions sont précisées dans la console lors de l'utilisation de l'objet /!\

- ##  routes()
```bash
  public function routes()
  {
      $this->file = fopen('./config/routes.php','r+');
      fseek($this->file, -2, SEEK_END);
      echo self::COLORS['YELLOW']."Input the routes (as 'url' -> array('url','...','...'))".self::COLORS['RESET']."\n";
      echo self::COLORS['RED']. "Input the url :".self::COLORS['RESET']." \n";
      $this->url = trim(fgets(STDIN));
      echo self::COLORS['RED']. "Input the app's name :".self::COLORS['RESET']." \n";
      $this->app = trim(fgets(STDIN));
      echo self::COLORS['RED']. "Input the method's name :".self::COLORS['RESET']." \n";
      $this->method = trim(fgets(STDIN));
      fwrite($this->file, "    array('$this->url','$this->app','$this->method'),\n");
      fwrite($this->file,');');
  }
```

Cette méthode permet de remplir routes.php. Il ajoutera un array à la fin du fichier avec 3 prompts (url,app,method) qui seront essentielles à la suite.
D'où l'appel de celle-ci en premier.

- ##  controller()

```bash

    public function controller()
    {
        $dirController = './src/Controller/';
        echo self::COLORS['YELLOW']."Input name (as 'exemple' -> ExempleController)".self::COLORS['RESET']."\n";
        echo self::COLORS['RED']."Controllers name :".self::COLORS['RESET']."\n";
        $controller_name = trim(ucfirst(fgets(STDIN)));
        $controller_nameController = $controller_name.'Controller';
        echo self::COLORS['RED']."-----------".self::COLORS['RESET']."\n";
        echo $controller_nameController."\n";

        $dataController = '<?php
            
            namespace App\Controller;
            
            use Core\Kernel\AbstractController;
            
            /**
             *
             */
            class '.$controller_nameController.' extends AbstractController
            {
                public function index()
                {
                    $message = "'.$controller_name.' Page";
                    //$this->dump($message);
                    $this->render("app.'.$this->app.'.'.$this->url.'",array(
                        "message" => $message,
                    ));
                }
            }
            ;';
            
                    $this->dirTemplateApp = "./template/app/$this->app/";
                    $this->dataTemplateApp = '<h1 style="text-align: center;font-size:33px;margin: 100px 0;color:#A67153;">
                <?= $message; ?>
            </h1>';

        file_put_contents($dirController.$controller_nameController.'.php', $dataController);
    }
```

Cette méthode permet de créer un Controller avec le nom définit à la fin de l'objet.
Les routes seront remplies directement à l'aide des premiers prompts définis dans routes().
Un index simple est créé à l'intérieur du Controller.


- ##  template()

```bash
 public function template()
{
    if (is_dir($this->dirTemplateApp)) {
        file_put_contents($this->dirTemplateApp.$this->url.'.php', $this->dataTemplateApp);
    } elseif (is_dir($this->app) === false) {
        mkdir($this->dirTemplateApp, 0700);
        echo self::COLORS['GREEN']."#### template/app/$this->app directory created.. ####".self::COLORS['RESET']."\n";
        file_put_contents($this->dirTemplateApp.$this->url.'.php', $this->dataTemplateApp);
    }
}
```

Cette méthode permet de créer dans template/app/ un dossier (si n'existe pas déjà) ainsi qu'une page à l'intérieur.
Un message se présentera dans le terminal pour montrer le dossier créé.

- ## Futur

Je compte plus tard ajouter une création de CRUD complète et personnalisable. Ou l'on pourrait facilement ajouter les inputs souhaités et insérer des inputs à l'interieur des formulaires en faisant un update qui recupererais les fichier pour insérer.

Aussi j'aimerais améliorer la méthode qui remplit la route car celle-ci n'est encore que très primaire. Bien que fonctionnelle, elle est limitée par la facon dont elle est écrite.

## Authors
- [@hugocadet76750](https://www.github.com/hugocadet76750)
