<?php
//  php bin/console.php make controller
require ('core/Console/MakeCRUD.php');

const COLOR_RESET = "\033[0m";
const COLOR_RED = "\033[91m";
const COLOR_GREEN = "\033[32m";
const COLOR_YELLOW = "\033[93m";


echo "\n";
echo COLOR_RED."##### CRUD Creator #####".COLOR_RESET."\n";
echo "\n";

$createController = new MakeCRUD($argv);

