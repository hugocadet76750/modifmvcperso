<?php

namespace App\Controller;

use Core\Kernel\AbstractController;

/**
 *
 */
class ContactController extends AbstractController
{
    public function index()
    {
        $message = "Contact Page";
        //$this->dump($message);
        $this->render("app.dossier.contact",array(
            "message" => $message,
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render("app.default.404");
    }
}
;